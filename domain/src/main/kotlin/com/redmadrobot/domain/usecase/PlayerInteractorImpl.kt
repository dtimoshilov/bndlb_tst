package com.redmadrobot.domain.usecase

import com.redmadrobot.domain.player.Player
import com.redmadrobot.domain.player.PlayerEvent
import com.redmadrobot.domain.repository.AudioFileRepository
import io.reactivex.Observable
import io.reactivex.Single
import java.io.*

class PlayerInteractorImpl (private val audioFileRepository: AudioFileRepository,
                            private val player: Player) : PlayerInteractor {

    override fun getFile(fileName: String): Single<File> {
        return audioFileRepository.getFile(fileName)
    }

    override fun getFileByteArray(file: File): ByteArray {
        val size = file.length().toInt()
        val bytes = ByteArray(size)
        try {
            val buf = BufferedInputStream(FileInputStream(file))
            buf.read(bytes, 0, bytes.size)
            buf.close()
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return bytes
    }

    override fun setFile(file: File): Observable<PlayerEvent> {
        player.setDataSource(file.absolutePath)
        return player.getPlayerStateObservable()
    }

    override fun play() {
        player.play()
    }

    override fun pause() {
        player.pause()
    }

    override fun stop() {
        player.stop()
    }

}