package com.redmadrobot.domain.usecase

import com.redmadrobot.domain.player.PlayerEvent
import io.reactivex.Observable
import io.reactivex.Single
import java.io.File

interface PlayerInteractor {

    fun getFile(fileName: String): Single<File>

    fun setFile(file: File): Observable<PlayerEvent>

    fun getFileByteArray(file: File): ByteArray

    fun play()

    fun pause()

    fun stop()

}