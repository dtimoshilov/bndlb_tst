package com.redmadrobot.domain.repository

import io.reactivex.Single
import java.io.File

interface AudioFileRepository {

    fun getFile(fileName: String): Single<File>

}