package com.redmadrobot.domain.player

enum class PlayerState {

    READY_TO_PLAY,

    PLAYING,

    STOPPED

}