package com.redmadrobot.domain.player

sealed class PlayerEvent

class ProgressEvent(val percent: Float): PlayerEvent()

class StopEvent: PlayerEvent()