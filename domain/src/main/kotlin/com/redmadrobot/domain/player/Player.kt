package com.redmadrobot.domain.player

import io.reactivex.Observable

interface Player {

    fun getPlayerStateObservable(): Observable<PlayerEvent>

    fun setDataSource(src: String)

    fun play()

    fun pause()

    fun stop()

}