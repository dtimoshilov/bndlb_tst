package com.redmadrobot.domain.player

class PlayerStateStore {

    var state = PlayerState.STOPPED

    val isReadyToPlay: Boolean
        @Synchronized get() = state == PlayerState.READY_TO_PLAY

    val isPlaying: Boolean
        @Synchronized get() = state == PlayerState.PLAYING

    val isStopped: Boolean
        @Synchronized get() = state == PlayerState.STOPPED

}