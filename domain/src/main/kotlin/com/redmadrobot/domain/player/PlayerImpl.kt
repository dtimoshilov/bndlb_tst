package com.redmadrobot.domain.player

import android.media.*
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class PlayerImpl : Player, Runnable {

    private val updateSubject: PublishSubject<PlayerEvent> = PublishSubject.create()

    private val state = PlayerStateStore()
    private var sourcePath: String? = null
    private var stop = false

    private val lock = java.lang.Object()

    override fun getPlayerStateObservable(): Observable<PlayerEvent> {
        return updateSubject
    }

    override fun setDataSource(src: String) {
        sourcePath = src
    }

    override fun play() {
        when (state.state) {
            PlayerState.READY_TO_PLAY -> {
                state.state = PlayerState.PLAYING
                syncNotify()
            }
            PlayerState.PLAYING -> {
                // do nothing
            }
            PlayerState.STOPPED -> {
                stop = false
                Thread(this).start()
            }
        }
    }

    override fun stop() {
        stop = true
    }

    override fun pause() {
        state.state = PlayerState.READY_TO_PLAY
    }

    private fun syncNotify() = synchronized(lock) {
        lock.notify()
    }

    private fun checkForPause() = synchronized(lock) {
        while (state.state == PlayerState.READY_TO_PLAY) {
            try {
                lock.wait()
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }

        }
    }

    override fun run() {
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO)

        val extractor = MediaExtractor()
        sourcePath?.let { extractor.setDataSource(it) }
        val format = extractor.getTrackFormat(0)

        val mime = format.getString(MediaFormat.KEY_MIME)
        val sampleRate = format.getInteger(MediaFormat.KEY_SAMPLE_RATE)
        val channels = format.getInteger(MediaFormat.KEY_CHANNEL_COUNT)
        val duration = format.getLong(MediaFormat.KEY_DURATION)

        val codec = MediaCodec.createDecoderByType(mime)
        codec.configure(format, null, null, 0)
        codec.start()

        val codecInputBuffers = codec.inputBuffers
        val codecOutputBuffers = codec.outputBuffers

        val channelConfiguration = if (channels == 1) AudioFormat.CHANNEL_OUT_MONO else AudioFormat.CHANNEL_OUT_STEREO
        val minSize = AudioTrack.getMinBufferSize(sampleRate, channelConfiguration, AudioFormat.ENCODING_PCM_16BIT)
        val audioTrack = AudioTrack(AudioManager.STREAM_MUSIC, sampleRate, channelConfiguration,
                AudioFormat.ENCODING_PCM_16BIT, minSize, AudioTrack.MODE_STREAM)

        audioTrack.play()
        extractor.selectTrack(0)

        val timeOutUs: Long = 1000
        val info = MediaCodec.BufferInfo()
        var sawInputEOS = false
        var sawOutputEOS = false
        var noOutputCounter = 0
        val noOutputCounterLimit = 10

        state.state = PlayerState.PLAYING
        while (!sawOutputEOS && noOutputCounter < noOutputCounterLimit && !stop) {
            checkForPause()

            noOutputCounter++

            if (!sawInputEOS) {
                val inputBufIndex = codec.dequeueInputBuffer(timeOutUs)
                if (inputBufIndex >= 0) {
                    val dstBuf = codecInputBuffers[inputBufIndex]
                    var sampleSize = extractor.readSampleData(dstBuf, 0)
                    if (sampleSize < 0) {
                        sawInputEOS = true
                        sampleSize = 0
                    }
                    val percents = extractor.sampleTime.toFloat() / duration
                    updateSubject.onNext(ProgressEvent(percents))
                    codec.queueInputBuffer(inputBufIndex, 0, sampleSize, extractor.sampleTime, if (sawInputEOS) MediaCodec.BUFFER_FLAG_END_OF_STREAM else 0)
                    if (!sawInputEOS) extractor.advance()
                }
            }

            val res = codec.dequeueOutputBuffer(info, timeOutUs)

            if (res >= 0) {
                if (info.size > 0) noOutputCounter = 0
                val buf = codecOutputBuffers[res]

                val chunk = ByteArray(info.size)
                buf.get(chunk)
                buf.clear()
                if (chunk.isNotEmpty()) audioTrack.write(chunk, 0, chunk.size)
                codec.releaseOutputBuffer(res, false)
                if (info.flags and MediaCodec.BUFFER_FLAG_END_OF_STREAM != 0) sawOutputEOS = true
            }
        }

        codec.stop()
        codec.release()
        audioTrack.flush()
        audioTrack.release()

        updateSubject.onNext(StopEvent())
        state.state = PlayerState.STOPPED
        stop = true
    }

}