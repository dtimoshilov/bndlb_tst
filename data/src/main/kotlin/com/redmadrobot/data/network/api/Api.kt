package com.redmadrobot.data.network.api

import io.reactivex.Single
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url

interface Api {

    @GET
    fun downloadFile(@Url fileName: String): Single<Response<ResponseBody>>

}