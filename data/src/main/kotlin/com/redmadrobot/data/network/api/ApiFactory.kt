package com.redmadrobot.data.network.api

import com.redmadrobot.data.network.ClientFactory
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import javax.inject.Inject

class ApiFactory @Inject constructor(private val clientFactory: ClientFactory) {

    fun <T> create(clazz: Class<T>, apiUrl: String): T {
        val builder = Retrofit.Builder()
        return builder.baseUrl(apiUrl)
                .client(clientFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build().create(clazz)
    }

}