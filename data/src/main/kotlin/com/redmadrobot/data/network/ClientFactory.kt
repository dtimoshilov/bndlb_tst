package com.redmadrobot.data.network

import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit

class ClientFactory {

    private val CONNECT_TIMEOUT_MILLIS = 5000L //5 seconds
    private val READ_TIMEOUT_MILLIS = 30000L //30 seconds
    private val WRITE_TIMEOUT_MILLIS = 10000L //10 seconds

    fun create() = buildClientWith()

    private fun buildClientWith(connectTimeOut: Long = CONNECT_TIMEOUT_MILLIS,
                                readTimeOut: Long = READ_TIMEOUT_MILLIS,
                                writeTimeOut: Long = WRITE_TIMEOUT_MILLIS): OkHttpClient? {
        val builder = OkHttpClient.Builder()

        with(builder) {
            connectTimeout(connectTimeOut, TimeUnit.MILLISECONDS)
            readTimeout(readTimeOut, TimeUnit.MILLISECONDS)
            writeTimeout(writeTimeOut, TimeUnit.MILLISECONDS)
        }

        return builder.build()
    }

}
