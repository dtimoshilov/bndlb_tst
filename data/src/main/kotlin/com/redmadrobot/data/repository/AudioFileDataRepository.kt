package com.redmadrobot.data.repository

import android.os.Environment
import com.redmadrobot.data.network.api.Api
import com.redmadrobot.domain.repository.AudioFileRepository
import io.reactivex.Single
import okhttp3.ResponseBody
import okio.Okio
import retrofit2.Response
import java.io.File
import java.io.IOException

class AudioFileDataRepository(private val api: Api) : AudioFileRepository {

    override fun getFile(fileName: String): Single<File> {
        return api.downloadFile(fileName)
                .flatMap { saveFile(fileName, it) }
    }

    private fun saveFile(fileName: String, response : Response<ResponseBody>): Single<File> {
        return Single.create { subscriber ->
            try {
                val file = File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_MUSIC).absoluteFile, fileName.replace("/", ""))
                val sink = Okio.buffer(Okio.sink(file))
                sink.writeAll(response.body().source())
                sink.close()
                subscriber.onSuccess(file)
            } catch (e: IOException) {
                e.printStackTrace()
                subscriber.onError(e)
            }
        }
    }

}