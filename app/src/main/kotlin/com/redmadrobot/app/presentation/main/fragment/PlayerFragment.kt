package com.redmadrobot.app.presentation.main.fragment

import android.Manifest
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.redmadrobot.app.presentation.base.fragment.BaseFragment
import com.redmadrobot.app.presentation.main.presenter.PlayerPresenter
import com.redmadrobot.app.presentation.main.view.PlayerView
import com.redmadrobot.templateproject.R
import kotlinx.android.synthetic.main.fragment_player.*
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.OnNeverAskAgain
import permissions.dispatcher.OnPermissionDenied
import permissions.dispatcher.RuntimePermissions

@RuntimePermissions
class PlayerFragment : BaseFragment(), PlayerView {

    companion object {

        val TAG = "PlayerFragment"

        fun newInstance(): PlayerFragment {
            val fragment = PlayerFragment()

            val args = Bundle()
            fragment.arguments = args

            return fragment
        }

    }

    @InjectPresenter
    internal lateinit var presenter: PlayerPresenter

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_player, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        PlayerFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults)
    }

    override fun checkExternalStoragePermission() {
        PlayerFragmentPermissionsDispatcher.requestExternalStoragePermissionWithCheck(this)
    }

    override fun setErrorState() {
        fragment_player_button_try_again.visibility = View.VISIBLE
        fragment_player_container_player_buttons.visibility = View.GONE
    }

    override fun setPlayerState() {
        fragment_player_button_try_again.visibility = View.GONE
        fragment_player_container_player_buttons.visibility = View.VISIBLE
    }

    override fun play() {
        setPauseAvailableState()
    }

    override fun pause() {
        setPlayAvailableState()
    }

    override fun stop() {
        setPlayAvailableState()
    }

    override fun showWaveForm(bytes: ByteArray) {
        fragment_player_waveform.updateVisualizer(bytes)
    }

    override fun setProgress(percent: Float) {
        fragment_player_waveform.updatePlayerPercent(percent)
    }

    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun requestExternalStoragePermission() {
        presenter.onPermissionGranted()
    }

    @OnPermissionDenied(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun onExternalStoragePermissionDenied() {
        presenter.onPermissionDenied()
    }

    @OnNeverAskAgain(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun onExternalStoragePermissionNeverAsk() {
        presenter.onPermissionNeverAsk()
    }

    private fun initUi() {
        fragment_player_button_try_again.setOnClickListener { presenter.onStartPlayerPrepare() }
        fragment_player_button_play.setOnClickListener { presenter.onPlay() }
        fragment_player_button_pause.setOnClickListener { presenter.onPause() }
        fragment_player_button_stop.setOnClickListener { presenter.onStop() }

        setPlayAvailableState()
    }

    private fun setPlayAvailableState() {
        fragment_player_button_pause.visibility = View.GONE
        fragment_player_button_play.visibility = View.VISIBLE
    }

    private fun setPauseAvailableState() {
        fragment_player_button_pause.visibility = View.VISIBLE
        fragment_player_button_play.visibility = View.GONE
    }

}