package com.redmadrobot.app.presentation.main.activity

import android.os.Bundle
import com.redmadrobot.app.presentation.base.activity.BaseActivity
import com.redmadrobot.app.presentation.main.fragment.PlayerFragment
import com.redmadrobot.templateproject.R

class MainActivity : BaseActivity() {

    companion object {
        private const val TAG = "MainActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            val fragment = PlayerFragment.newInstance()
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.activity_main_container_fragment, fragment)
                    .commit()
        }
    }

}
