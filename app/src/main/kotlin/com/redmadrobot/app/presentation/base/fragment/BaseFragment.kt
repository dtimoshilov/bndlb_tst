package com.redmadrobot.app.presentation.base.fragment

import android.support.v4.app.DialogFragment
import com.arellomobile.mvp.MvpAppCompatFragment
import com.redmadrobot.app.presentation.base.view.BaseView
import com.redmadrobot.app.presentation.main.fragment.SpinnerDialog

abstract class BaseFragment : MvpAppCompatFragment(), BaseView {

    override fun showLoading() {
        hideLoading()
        SpinnerDialog().show(childFragmentManager, SpinnerDialog.TAG)
        childFragmentManager.executePendingTransactions()
    }

    override fun hideLoading() {
        val currentDialog = childFragmentManager.findFragmentByTag(SpinnerDialog.TAG)
        (currentDialog as? DialogFragment)?.dismiss()
        childFragmentManager.executePendingTransactions()
    }

}

