package com.redmadrobot.app.presentation.main.view

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.redmadrobot.app.presentation.base.view.BaseView

interface PlayerView : BaseView {

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun checkExternalStoragePermission()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun setErrorState()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun setPlayerState()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun play()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun pause()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun stop()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showWaveForm(bytes: ByteArray)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun setProgress(percent: Float)

}