package com.redmadrobot.app.presentation.main.presenter

import com.arellomobile.mvp.InjectViewState
import com.redmadrobot.app.internal.di.DI
import com.redmadrobot.app.presentation.base.presenter.BasePresenter
import com.redmadrobot.app.presentation.main.view.PlayerView
import com.redmadrobot.data.extension.schedulersIoToMain
import com.redmadrobot.domain.player.ProgressEvent
import com.redmadrobot.domain.player.StopEvent
import com.redmadrobot.domain.usecase.PlayerInteractor
import java.io.File
import javax.inject.Inject

@InjectViewState
class PlayerPresenter : BasePresenter<PlayerView>() {

    companion object {
        private val TAG = "PlayerPresenter"

        private val FILE_NAME = "e6f38e4b-7467-f20f-8221-4fc90b3999cc/e6f38e4b-7467-f20f-8221-4fc90b3999cc.mp3"
    }

    @Inject
    internal lateinit var interactor: PlayerInteractor

    init {
        DI.app.provideComponent().inject(this)
        onStartPlayerPrepare()
    }

    fun onStartPlayerPrepare() {
        viewState.checkExternalStoragePermission()
    }

    fun onPermissionGranted() {
        downloadFile()
    }

    fun onPermissionDenied() {
        viewState.setErrorState()
    }

    fun onPermissionNeverAsk() {
        //TODO set some new state with instruction how to enable permission from settings
        viewState.setErrorState()
    }

    fun onPlay() {
        interactor.play()
        viewState.play()
    }

    fun onPause() {
        interactor.pause()
        viewState.pause()
    }

    fun onStop() {
        interactor.stop()
        viewState.stop()
    }

    private fun downloadFile() {
        viewState.showLoading()
        interactor.getFile(FILE_NAME)
                .schedulersIoToMain()
                .subscribe(
                        { file ->
                            viewState.hideLoading()
                            viewState.setPlayerState()
                            viewState.showWaveForm(interactor.getFileByteArray(file))
                            setFileForPlaying(file)
                        },
                        {
                            viewState.hideLoading()
                            viewState.setErrorState()
                        }
                )
                .disposeOnPresenterDestroy()
    }

    private fun setFileForPlaying(file: File) {
        interactor.setFile(file)
                .schedulersIoToMain()
                .subscribe {
                    when (it) {
                        is ProgressEvent -> viewState.setProgress(it.percent)
                        is StopEvent -> viewState.stop()
                    }
                }
                .disposeOnPresenterDestroy()
    }

}