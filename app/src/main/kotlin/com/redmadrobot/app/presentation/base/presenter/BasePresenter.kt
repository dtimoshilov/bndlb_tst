package com.redmadrobot.app.presentation.base.presenter

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

open class BasePresenter<T> : MvpPresenter<T>() where T : MvpView {

    private val compositeDisposable by lazy { CompositeDisposable() }

    fun dispose() {
        compositeDisposable.dispose()
    }

    fun clear() {
        compositeDisposable.clear()
    }

    override fun onDestroy() {
        super.onDestroy()
        dispose()
    }

    fun Disposable.disposeOnPresenterDestroy(): Disposable {
        compositeDisposable.add(this)
        return this
    }
}