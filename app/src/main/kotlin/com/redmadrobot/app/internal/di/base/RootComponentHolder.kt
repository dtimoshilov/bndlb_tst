package com.redmadrobot.app.internal.di.base

@Suppress("unused")
abstract class RootComponentHolder<out Component> : ComponentHolder<Component> {

    protected abstract fun provideInternal(): Component

    @Volatile
    private var pair: Pair<Component, Int>? = null

    override fun provideComponent(): Component {
        synchronized(this) {
            val localPair = pair
            val component: Component
            if (localPair == null) {
                component = provideInternal()
                pair = Pair(component, 1)
            } else {
                component = localPair.first
                pair = Pair(component, localPair.second + 1)
            }
            return component
        }
    }

    fun destroyComponent() {
        synchronized(this) {
            pair = null
        }
    }
}