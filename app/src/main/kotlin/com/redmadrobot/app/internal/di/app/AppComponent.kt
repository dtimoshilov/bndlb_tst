package com.redmadrobot.app.internal.di.app

import com.redmadrobot.app.presentation.main.presenter.PlayerPresenter
import dagger.Component

@AppScope
@Component(modules = arrayOf(AppModule::class))
interface AppComponent {

    fun inject(entity: PlayerPresenter)

}