package com.redmadrobot.app.internal.di.app

import android.content.Context
import com.redmadrobot.app.internal.di.base.RootComponentHolder

class AppComponentHolder(private val context: Context)
    : RootComponentHolder<AppComponent>() {

    override fun provideInternal(): AppComponent {
        return DaggerAppComponent
                .builder()
                .appModule(AppModule(context))
                .build()
    }

}