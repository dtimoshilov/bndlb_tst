package com.redmadrobot.app.internal.di.base

interface ComponentHolder<out Component> {

    fun provideComponent(): Component

}