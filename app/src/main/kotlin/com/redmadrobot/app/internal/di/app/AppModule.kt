package com.redmadrobot.app.internal.di.app

import android.content.Context
import com.redmadrobot.domain.player.Player
import com.redmadrobot.domain.player.PlayerImpl
import com.redmadrobot.data.network.ClientFactory
import com.redmadrobot.data.network.api.Api
import com.redmadrobot.data.network.api.ApiFactory
import com.redmadrobot.data.repository.AudioFileDataRepository
import com.redmadrobot.domain.repository.AudioFileRepository
import com.redmadrobot.domain.usecase.PlayerInteractor
import com.redmadrobot.domain.usecase.PlayerInteractorImpl
import dagger.Module
import dagger.Provides

@Module
class AppModule(private val context: Context) {

    @Provides
    @AppScope
    fun provideContext(): Context = context

    @Provides
    @AppScope
    fun provideClientFactory(): ClientFactory = ClientFactory()

    @Provides
    @AppScope
    fun provideApiFactory(clientFactory: ClientFactory): ApiFactory = ApiFactory(clientFactory)

    @Provides
    @AppScope
    fun provideApi(apiFactory: ApiFactory): Api =
            apiFactory.create(Api::class.java, "https://static.bandlab.com/revisions-formatted/")

    @Provides
    @AppScope
    fun provideAudioFileRepository(api: Api): AudioFileRepository = AudioFileDataRepository(api)

    @Provides
    @AppScope
    fun providePlayerInteractor(audioFileRepository: AudioFileRepository, player: Player): PlayerInteractor
            = PlayerInteractorImpl(audioFileRepository, player)

    @Provides
    @AppScope
    fun providePlayer(): Player = PlayerImpl()

}