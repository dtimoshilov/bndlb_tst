package com.redmadrobot.app.internal.di

import android.annotation.SuppressLint
import android.content.Context
import com.redmadrobot.app.internal.di.app.AppComponentHolder

//DI contains all component, so it should contain context
@SuppressLint("StaticFieldLeak")
object DI {
    private lateinit var context: Context

    fun init(context: Context) {
        this.context = context
    }

    val app: AppComponentHolder by lazy {
        AppComponentHolder(context)
    }
}