package com.redmadrobot.app

import android.app.Application
import android.content.Context
import com.redmadrobot.app.internal.di.DI

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        buildDependencyGraph(this)
    }

    private fun buildDependencyGraph(context: Context) {
        DI.init(context)
    }
}